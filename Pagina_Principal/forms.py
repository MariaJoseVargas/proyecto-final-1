from django.forms import CharField, Form

class BuscadorMamiferos(Form):
    nombre=CharField(min_length=4 ,max_length=20)
    color=CharField(min_length=4 ,max_length=20)
    tamaño=CharField(min_length=4 ,max_length=10)
    tipo=CharField(min_length=4 ,max_length=10)
