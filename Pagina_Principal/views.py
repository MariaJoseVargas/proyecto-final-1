from django.shortcuts import render
from django.views import View
from .forms import BuscadorMamiferos

class Principal(View):
    def get(self, request):

        datos={
            'titulo': 'Fauna Mosquera',
            'bienvenida': '¡BIENVENIDOS!',
            'encabezado': 'Humedal Guali, Mosquera - Cundinamarca',
            'menuEspecies': 'ESPECIES',
            'menuEquipo': 'Equipo',
            'menuContacto': 'Contacto',
            'servicios': 'SERVICIOS',
            'especiesTitulo': 'ESPECIES',
            'especiesAves': 'AVES',
            'especiesMamiferos': 'MAMIFEROS',
            'especiesReptiles': 'REPTILES',
            'especiesAvesTexto': 'Las aves existen de diferentes formas, tamaños, especímenes, etc. En Mosquera no es la excepción, aquí encontrarás las que existen en este territorio.',
            'especiesMamiferosTexto': 'En Mosquera existe una gran variedad de mamíferos tanto hervivoros como carnívoros, en su gran mayoría cubiertos de pelo,  aquí encontrarás los que existen en este territorio.',
            'especiesReptilesTitulo': 'Especie piel recubierta con escamas o caparazón duro, se desplazan arrastrándose. Quieres saber más información de los reptiles de Mosquera, aquí encontrarás los que existen en este territorio.',
            'tituloEquipo': 'EQUIPO',
            'equipoNoath': 'Noath Muñoz',
            'codigo1': '20182025035',
            'equipoGabriel':'Gabriel Molina',
            'codigo2': '20191025123',
            'equipoJohan': 'Johan Suárez',
            'codigo3': '20192025040',
            'equipoMaria': 'María José Vargas',
            'codigo4': '20192025122',
            'equipoXimena': 'Ximena Nitola',
            'codigo5': '20201025048',
            'contactoTitulo': 'CONTACTO'
        }
        return render(request, 'principal/principal.html', datos)

class Mamiferos(View):

    def rbm(request):

        BM=BuscadorMamiferos()

        datos={
            "form":BM
        }

        BuscadorM=render(request,"FormsMamiferos.html",datos)
        return BuscadorM
