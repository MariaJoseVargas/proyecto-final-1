from django.db.models import *

class EspeciesReptiles(Model):
    nombre=CharField(max_length=20)
    color=CharField(max_length=20)
    tamaño=CharField(max_length=10)
    tipo=CharField(max_length=10)

class EspeciesMamiferos(Model):
    nombre=CharField(max_length=20)
    color=CharField(max_length=20)
    tamaño=CharField(max_length=10)
    tipo=CharField(max_length=10)

class EspeciesAves(Model):
    nombre=CharField(max_length=20)
    color=CharField(max_length=20)
    tamaño=CharField(max_length=10)
    tipo=CharField(max_length=10)

class EspeciesRiesgo(Model):
    nombre=CharField(max_length=20)
    cantidad=FloatField(max_length=100)
    tamaño=CharField(max_length=10)
    tipo=CharField(max_length=10) 
    familia=CharField(max_length=20)   

class ZonasProtegidas(Model):
    localizacion=CharField(max_length=50)
    ambiente=CharField(max_length=50)
    tamaño=CharField(max_length=50)

class Humedales(Model):
    nombre=CharField(max_length=20)
    ubicacion=CharField(max_length=50)
    tamaño=CharField(max_length=10)
    tipo_humedal=CharField(max_length=10)    